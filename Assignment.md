**Assignment: Setting up Jenkins, GitLab, and Creating CI/CD Pipelines**

**Part 1: Setting Up Jenkins Locally**

1. Setup Jenkins master on your local machine. Access the Jenkins UI through a web browser and complete the initial setup.

**Part 2: Setting Up GitLab and Forking Repository**

1. Create an account on GitLab if you don't have one.
2. Fork the repository: https://gitlab.com/pankaj.y/nht-ci to your GitLab account.

**Part 3: Jenkins Pipeline for Node.js Application**

1. Inside your forked repository on GitLab, create a `Jenkinsfile` that accomplishes the following:
   - Clones the repository.
   - Builds a Node.js application (node-hello-world).
   - Creates a Docker image of the application.
   - Pushes the Docker image to your Docker Hub account.
   - Ensure the Jenkins pipeline is triggered automatically upon each commit.

**Part 4: GitLab CI/CD for tcp-echo-server App**

1. Inside the same forked repository on GitLab, create a `.gitlab-ci.yml` file with the following specifications:
     - Installs dependencies for the tcp-echo-server app.
     - Builds the app (tcp-echo-server).
     - Creates a Docker image of the app.
     - Pushes the Docker image to your Docker Hub account.
     - Ensure this GitLab CI/CD pipeline is triggered automatically upon each commit.
     - Set up your own GitLab runners and associate them with your repository.

**Bonus Part: Adding Jenkins Agent and Using CASC Plugin**

1. Configure and connect a Jenkins agent (slave) with the master node through Jenkins UI ( Manage Jenkins).
2. Install the "Configuration as Code" (CASC) plugin and use it to configure and connect the Jenkins agent.
3. Utilize the configured Jenkins agent to run your pipeline jobs.


**Submission:**
- Share your forked gitlab repo link which should contain the following:
  - Documentation that includes a brief explanation of your Jenkins pipeline, GitLab CI/CD configuration, and any setup details with proper screenshots attached.
  - Jenkinsfile and .gitlab-ci file should be present in your repo only.
  - Optionally, provide insights into any challenges faced and the solutions applied.

Note: 
1. The focus of this assignment is on hands-on practice and problem-solving. Avoid sharing direct answers, but feel free to seek guidance or ask questions if you encounter difficulties.
2. We will not consider the commits made after the deadline i.e. 21 Aug, 2023 11.59 PM IST.
3. All the references, scripts, example pipelines and code is available in the shared repo itself.